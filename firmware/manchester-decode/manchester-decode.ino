

#define INPUTPIN A1 // aka PC1, aka digital pin 15
// we must use these registers and interrupt numbers: (if the above pin changes all of these have to be checked in the datasheet!)
// PCINT9 in the PCMSK1 register
// PCIE1 in the PCICR register
// PCIR1 
// PC (port c)
// PCI1 interupt vector will be called


void setup() {
	Serial.begin(115200);
	Serial.println("Hello World!");
	pinMode(INPUTPIN, INPUT);
	// attach flank interrupt:
	PCICR  |= (1<<PCIE1);								// enable interrupts on interrupt handler 1 (used for most of port c)
	PCMSK1 |= (1<<PCINT9);								// enable interrupts from pin PC1, so other pins in the same group don't trigger the interrupt
}




#define BUFSIZE 100 									// actually opentherm 2.2 specifies a fixes packet size of 34 bits (32 data and one opening and closing 1)
uint8_t  data_buffer[BUFSIZE];							// buffer for some bits (x8 overhead)
uint8_t endofpacket = 0;								// first bit that is not part of the packet
bool expectshortpulse = false;							// boolean flag to tell if we are halfway a bit
unsigned long last_change = 0;							// uS of previous flank


// one bit is stored on the first flank of each bit. If the bits in case is the same as the one before this will be immediately at the start of the bit and the previous flank will be 500uS before
// if the bit is opposite of the previous bit the first flank of the bit will be halfway in and the last change will have been 1000uS(1ms) before. In the former case the bit has just started and we ignore 
// one future flank. Notice how we never have to read the polarity of the flank nor the actual level. All data is contained in the timing of the signal. Only the first bit cannot be decided in this way 
// but opentherm ensures that each packets starts (and ends) with a one-bit.
// see: http://www.atmel.com/images/atmel-9164-manchester-coding-basics_application-note.pdf
// there are a few things different here. 
// 1. idle state for opentherm is low instead of high
// 2. we don't use the internal timer capture because our input is not on the right pin
// 3. fortunately we can accurately time using the microSeconds() function provided by the arduino (effectively still using a timer but with some added code)

// _______1 0 0 1 1 1 0 1 1 
// _______^__^_^^_^_^__^^_^__________
SIGNAL(PCINT1_vect) {
	unsigned long now = micros(); 						// although micros overflows every 17 minutes the following is safe since the subtraction will overflow back
	unsigned long interval = now-last_change;
	if(interval < 250) {  								// error: pulse too short
		return; 										// should never happen, just ignore
	}
	else if(interval < 750) {							// short pulse 
		if(expectshortpulse){							// this is the one we have been waiting for, do nothing
			expectshortpulse = false;					// don't necessarily expect a short pulse next
		} else {
			data_buffer[endofpacket] = data_buffer[endofpacket-1];
			endofpacket++;
			expectshortpulse = true;// the next one should be short
		}	
	}
	else if(interval < 1250) {							// long pulse
		if(expectshortpulse) {
			// error got a long pulse where a short one should have been...
		}
		data_buffer[endofpacket] = ! data_buffer[endofpacket-1];
		endofpacket++;
	} 
	else {												// very long, this is the start of a new packet
		endofpacket = 0;								// reset packet, should not be necessary as the loop() empties the packet
		// if(current pin value==LOW) => error, must start with 1
		data_buffer[endofpacket] = 1;					// first bit is always 1 as per opentherm spec
		endofpacket++;
		expectshortpulse = true;
	}

	last_change = now;
	
	// clear interrupt flag: 							// this bit prevents further interrupts until it is cleared (which, counter-intuitively happens by writing 1 to it)
	// PCIFR|=(1<<PCIF1);    							// or so I thought, but apparently it is not needed...?
}



void loop() {
	if(endofpacket > 0 && micros()-last_change > 5000) { 	// don't start doing serial before the whole packet is received, it will interfere!!
		for(int i=0;i<endofpacket;i++) {
			Serial.print(data_buffer[i]);
			Serial.print(i==(endofpacket-1)?"\n":",");
		}			
		endofpacket = 0;									// empty the packet buffer
	}
	delay(1);
}

