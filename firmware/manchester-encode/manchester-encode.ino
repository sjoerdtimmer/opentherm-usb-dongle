

#define OUTPUTPIN 2

void setup() {
	Serial.begin(115200);
	Serial.println("Hello World!");
	pinMode(OUTPUTPIN, OUTPUT);
	digitalWrite(OUTPUTPIN, LOW);
}


void loop() {

	bool msg[34] = {
		1, // signalling bit

		1,0,0,0,1,1,1,1,
		1,0,1,1,0,0,1,1,
		0,0,0,0,0,0,0,0,
		0,0,1,0,1,1,1,1,
		
		1 // additional closing bit
	};

	for(int i=0;i<34;i++){
		// Serial.println(msg[i]);
		digitalWrite(OUTPUTPIN, msg[i]);
		delayMicroseconds(500);
		digitalWrite(OUTPUTPIN, !msg[i]);
		delayMicroseconds(500);
	}

	delay(1000);
}
